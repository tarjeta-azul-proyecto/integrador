$(document).ready(function () {
  /**Delcaracion de la Clase Datos para guardar de forma global los datos de los formularios **/
  class Datos {
    nombre;
    apellido;
    email;
    fecNac;
    dni;
    celular;
    ciudad;
    provincia;
    tipo;
    categoria;
    limite;
    fecVto;
  }

  /** Instancia de la clase Datos **/
  let d = new Datos();

  /** Funcion autoinvocada, se ejecuta al cargarse la pagina **/
  (() => {
    "use strict";

    /** Obtenemos los formulario de datos personales y datos de la tarjeta **/
    let bloque1 = $("#primeraParte");
    let bloque2 = $("#segundaParte");

    /** Localizamos los formularios que poseen la clase 'needs-validation' **/
    const formularios = $(".needs-validation");

    /** Creamos un array 'Array'.
     *  Con forEach recorremos los formularios obtenidos en la linea 284
     *  Le agregamos un evento de escucha 'submit'. Cuando desde el formulario llamamos al evento submit, se ejecuta
     * el codigo dentro del 'form.addEventListener(..)'
     *  **/
    Array.from(formularios).forEach((form) => {
      form.addEventListener(
        "submit",
        (e) => {
          if (form.checkValidity() === false) {
            // Hubo un error en la validacion de datos de entrada. No avanza
            e.stopPropagation(); // Evita el envio del formulario con el envento submit
            e.preventDefault();
            form.classList.add("was-validated");
          } else {
            // No hubo errores en la validacion de datos de entrada. Avanza
            form.classList.add("was-validated");
            e.stopPropagation();
            e.preventDefault();

            /** Con el switch verificamos cual es el formulario que estamos iterando por medio del id y
             * realiza las acciones correspondientes
             *  **/
            switch (form.id) {
              case "primeraParte":
                bloque1.addClass("d-none"); // Ocultamos la vista del form de Datos Personales
                bloque2.removeClass("d-none"); // Mostramos la vista del form Datos de la Tarjeta
                break;
              case "segundaParte":
                /** Llamamos a la funcion CompletarFormulario() **/
                CompletarFormulario();
                break;
            }
          }
        },
        false
      );
    });
  })();

  /** Funcion que nos permite almacenar los valores ingresados por el usuario en la instancia 'd' de la Clase 'Datos',
   * para tenerlos guardados y mostrarlos en la pantalla de confirmacion de datos
   * **/
  function CompletarFormulario() {
    /** Almacenamos los datos de entrada en la instancia 'd' **/
    d.nombre = $("#nombre").val();
    d.apellido = $("#apellido").val();
    d.email = $("#email").val();
    d.fecNac = $("#fecNac").val();
    d.dni = $("#dni").val();
    d.celular = $("#celular").val();
    d.ciudad = $("#ciudad").val();
    d.provincia = $("#provincia").val();
    d.tipo = $("#tipo option:selected").val();
    d.categoria = $("#categoria option:selected").val();
    d.limite = $("#limite").val();
    d.fecVto = $("#fecVto").val();

    /** Mostramos los valores ingresados por el usuario en la pantalla de Confirmacion de Datos **/
    $("#nombreConfirmar").val(d.nombre);
    $("#apellidoConfirmar").val(d.apellido);
    $("#emailConfirmar").val(d.email);
    $("#fecNacConfirmar").val(d.fecNac);
    $("#dniConfirmar").val(d.dni);
    $("#celularConfirmar").val(d.celular);
    $("#ciudadConfirmar").val(d.ciudad);
    $("#provinciaConfirmar").val(d.provincia);
    $("#tipoConfirmar").val(d.tipo);
    $("#categoriaConfirmar").val(d.categoria);
    $("#limiteConfirmar").val(d.limite);
    $("#fecVtoConfirmar").val(d.fecVto);

    $("#segundaParte").addClass("d-none"); // Ocultamos la vista del form de Datos la Tarjeta
    $("#confirmarEnvio").removeClass("d-none"); // Mostramos la vista del form de confirmación
  }

  /*** FUNCIONES ***/
  $("#btnSegundaParteAtras").click(function () {
    $("#primeraParte").removeClass("d-none"); //Mostramos la vista anterior (Form Datos Personales)
    $("#segundaParte").addClass("d-none"); //Ocultamos la vista actual (Form Datos de la Tarjeta)
  });

  $("#btnVolverFormulario").click(function () {
    $("#primeraParte").removeClass("d-none"); //Mostramos la vista de Datos Personales
    $("#confirmarEnvio").addClass("d-none"); //Ocultamos la vista actual (Form Confirmacion de Datos)
  });

  $("#btnConfirmarDatos").click(function () {
    $("#contenedorJson").removeClass("d-none"); // Mostramos el contenedor de los datos a enviar al BackEnd
    /** d es la instancia de la case Datos{} de alcance global **/
    $("#json").html(JSON.stringify(d, null, 4));
    /** Funcion para mostrar por un X determinado de tiempo los datos a enviar al BackEnd **/
    setTimeout(() => {
      $("#contenedorJson").addClass("d-none"); // Ocultamos el contenedor de los datos a enviar al BackEnd
      $("#primeraParte")[0].reset(); // Reseteamos los input del fomrulario Datos Personales
      $("#segundaParte")[0].reset(); // Reseteamos los unput del formulario de Datos de la Tarjeta
      $("#primeraParte").removeClass("d-none"); //Mostramos la vista de Datos Personales
      $("#confirmarEnvio").addClass("d-none"); //Ocultamos la vista actual (Form Confirmacion de Datos)
    }, 3000);
  });
});

document.addEventListener("DOMContentLoaded", () => {
  // Escuchamos el click del botón
  const $boton = document.querySelector("#btnExportarPDF");
  $boton.addEventListener("click", () => {
    // <-- Aquí puedes elegir cualquier elemento del DOM para Exportar a PDF  -->
    // const $elementoParaConvertir = document.body;

    //  <-- Descomentar la linea siguiente y probar exportar solamente la tabla -->
    const $elementoParaConvertir = document.querySelector("#confirmarEnvio");

    html2pdf()
      .set({
        margin: 1,
        filename: "documento.pdf",
        image: {
          type: "png",
          quality: 0.98,
        },
        html2canvas: {
          scale: 3, // A mayor escala, mejores gráficos, pero más peso
          letterRendering: true,
          scrollY: 0
        },
        jsPDF: {
          unit: "in",
          format: "a3", //formato de hoja
          orientation: "portrait", // landscape o portrait (disposicion vertical u orizontal)
        },
      })
      .from($elementoParaConvertir)
      .save()
      .catch((err) => console.log(err));
  });
});



window.addEventListener('load', function() { //cuando la pagina carga todos los elementos
  //LA API DEVUELVE LAS COTIZACIONES PARA CIERTAS MONEDAS
  var settings = {
      "url": "https://www.dolarsi.com/api/api.php?type=valoresprincipales",
      "method": "GET",
      "timeout": 0
  };


  $.ajax(settings).done(function cotizaciones (response) { //función de ajax para hacer pedido (request)
      console.log("respuesta",response);
      jQuery.each(response, function(i, item){
          //iteramos sobre el array y agregamos los datos
          console.log(item.casa.nombre);
          $('#cotizaciones').append(`${item.casa.nombre} - Compra: ${item.casa.compra} - Venta ${item.casa.venta} </br>`);
      });
  });
}); 